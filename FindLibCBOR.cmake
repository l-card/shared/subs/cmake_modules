
if (NOT LIBCBOR_FOUND)
    if(WIN32)
        cmake_policy(VERSION 3.2)
    endif(WIN32)

    if(WIN32)
        string(REGEX REPLACE "\\\\" "/" PROGFILES    $ENV{PROGRAMFILES})
        string(REGEX REPLACE "\\\\" "/" PROGFILESX86 $ENV{PROGRAMFILES\(x86\)})

        set(LIBCBOR_SEARCH_DIRS ${LIBCBOR_ROOT_DIR} ${PROGFILES} ${PROGFILES}/libs ${PROGFILESX86} ${PROGFILESX86}/libs)
        set(LIBCBOR_SEARCH_INCLUDE_DIRS ${LIBCBOR_SEARCH_DIRS})
        set(LIBCBOR_SEARCH_LIBRARY_DIRS ${LIBCBOR_SEARCH_DIRS})
        set(LIBCBOR_SEARCH_INCLUDE_SUFFIX include libcbor/include cbor/include)
        if("${CMAKE_C_COMPILER_ID}" STREQUAL "MSVC")
            if(CMAKE_SIZEOF_VOID_P EQUAL 4)
                set(LIBCBOR_SEARCH_LIBRARY_DEF_SUFFIX "msvc")
            else(CMAKE_SIZEOF_VOID_P EQUAL 4)
                set(LIBCBOR_SEARCH_LIBRARY_DEF_SUFFIX "msvc64")
            endif(CMAKE_SIZEOF_VOID_P EQUAL 4)
        else("${CMAKE_C_COMPILER_ID}" STREQUAL "GNU")
            if(CMAKE_SIZEOF_VOID_P EQUAL 4)
                set(LIBCBOR_SEARCH_LIBRARY_DEF_SUFFIX "mingw")
            else(CMAKE_SIZEOF_VOID_P EQUAL 4)
                set(LIBCBOR_SEARCH_LIBRARY_DEF_SUFFIX "mingw64")
            endif(CMAKE_SIZEOF_VOID_P EQUAL 4)
        endif("${CMAKE_C_COMPILER_ID}" STREQUAL "MSVC")
        set(LIBCBOR_SEARCH_LIBRARY_SUFFIX   "lib/${LIBCBOR_SEARCH_LIBRARY_DEF_SUFFIX}"
                                          "libcbor/lib/${LIBCBOR_SEARCH_LIBRARY_DEF_SUFFIX}"
                                          "cbor/lib/${LIBCBOR_SEARCH_LIBRARY_DEF_SUFFIX}")
    else(WIN32)
        find_package(PkgConfig QUIET)
        pkg_check_modules(LIBCBOR_PKG QUIET libcbor)

        set(LIBCBOR_SEARCH_INCLUDE_DIRS  ${LIBCBOR_PKG_INCLUDE_DIRS} include local/include)

        if(CMAKE_SIZEOF_VOID_P EQUAL 4)
            set(LIBCBOR_SEARCH_LIBRARY_DIRS lib local/lib lib/i386-linux-gnu)
        else(CMAKE_SIZEOF_VOID_P EQUAL 4)
            set(LIBCBOR_SEARCH_LIBRARY_DIRS lib64 lib/x86_64-linux-gnu local/lib64)
        endif(CMAKE_SIZEOF_VOID_P EQUAL 4)
    endif(WIN32)

    find_path(LIBCBOR_INCLUDE_DIR NAMES cbor.h
       PATHS       
       ${LIBCBOR_SEARCH_INCLUDE_DIRS}
       PATH_SUFFIXES ${LIBCBOR_SEARCH_INCLUDE_SUFFIX}
    )

    find_library(LIBCBOR_LIBRARY NAMES cbor libcbor liblibcbor
       PATHS
       ${LIBCBOR_SEARCH_LIBRARY_DIRS}
       PATH_SUFFIXES ${LIBCBOR_SEARCH_LIBRARY_SUFFIX}
    )

    include(FindPackageHandleStandardArgs)
    find_package_handle_standard_args(LibCBOR
            REQUIRED_VARS LIBCBOR_INCLUDE_DIR LIBCBOR_LIBRARY
            )

    if(LIBCBOR_FOUND)
        set(LIBCBOR_LIBRARIES ${LIBCBOR_LIBRARY})
        set(LIBCBOR_INCLUDE_DIRS ${LIBCBOR_INCLUDE_DIR})
    endif(LIBCBOR_FOUND)
endif (NOT LIBCBOR_FOUND)

