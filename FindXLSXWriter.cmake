# - Try to find libmodbus
# Once done this will define
#
#  XLSXWRITER_FOUND - system has XLSXWRITER
#  XLSXWRITER_INCLUDE_DIRS - the XLSXWRITER include directory
#  XLSXWRITER_LIBRARIES - Link these to use XLSXWRITER

# Copyright (c) 2006, Jasem Mutlaq <mutlaqja@ikarustech.com>
# Based on FindLibfacile by Carsten Niehaus, <cniehaus@gmx.de>
#
# Redistribution and use is allowed according to the terms of the BSD license.
# For details see the accompanying COPYING-CMAKE-SCRIPTS file.

if (XLSXWRITER_INCLUDE_DIRS AND XLSXWRITER_LIBRARIES)
    # in cache already
    set(XLSXWRITER_FOUND TRUE)
    message(STATUS "Found libxlsxwriter: ${XLSXWRITER_LIBRARIES}")
else (XLSXWRITER_INCLUDE_DIRS AND XLSXWRITER_LIBRARIES)
    find_path(XLSXWRITER_INCLUDE_DIRS xlsxwriter.h
        PATH_SUFFIXES xlsxwriter
        ${_obIncDir}
        ${GNUWIN32_DIR}/include
    )

  find_library(XLSXWRITER_LIBRARIES NAMES xlsxwriter
    PATHS
    ${_obLinkDir}
    ${GNUWIN32_DIR}/lib
  )

    set(CMAKE_REQUIRED_INCLUDES ${XLSXWRITER_INCLUDE_DIRS})
    set(CMAKE_REQUIRED_LIBRARIES ${XLSXWRITER_LIBRARIES})

    if(XLSXWRITER_INCLUDE_DIRS AND XLSXWRITER_LIBRARIES)
        set(XLSXWRITER_FOUND TRUE)
    else (XLSXWRITER_INCLUDE_DIRS AND XLSXWRITER_LIBRARIES)
        set(XLSXWRITER_FOUND FALSE)
    endif(XLSXWRITER_INCLUDE_DIRS AND XLSXWRITER_LIBRARIES)

  if (XLSXWRITER_FOUND)
    if (NOT XLSXWRITER_FIND_QUIETLY)
      message(STATUS "Foud llibxlsxwriter: ${XLSXWRITER_LIBRARIES}")
    endif (NOT XLSXWRITER_FIND_QUIETLY)
  else (XLSXWRITER_FOUND)
    if (XLSXWRITER_FIND_REQUIRED)
      message(FATAL_ERROR "llibxlsxwriter not found.")
    endif (XLSXWRITER_FIND_REQUIRED)
  endif (XLSXWRITER_FOUND)

  mark_as_advanced(XLSXWRITER_INCLUDE_DIRS XLSXWRITER_LIBRARIES)
  
endif (XLSXWRITER_INCLUDE_DIRS AND XLSXWRITER_LIBRARIES)
