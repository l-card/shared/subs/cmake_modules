# - Find libdaemon library
# This module defines
#  LIBDAEMON_INCLUDE_DIRS, where to find libdaemon headers
#  LIBDAEMON_LIBRARIES, the libraries needed to libdaemon
#  LIBDAEMON_FOUND, If false, do not try to use libdaemon.
#

if(NOT LIBDAEMON_FOUND)
    find_package(PkgConfig QUIET)
    pkg_check_modules(LIBDAEMON_PKG QUIET libdaemon)

    set(LIBDAEMON_SEARCH_INCLUDE_DIRS  ${LIBDAEMON_PKG_INCLUDE_DIRS} include local/include)

    if(CMAKE_SIZEOF_VOID_P EQUAL 4)
        set(LIBDAEMON_SEARCH_LIBRARY_DIRS lib local/lib lib/i386-linux-gnu)
    else(CMAKE_SIZEOF_VOID_P EQUAL 4)
        set(LIBDAEMON_SEARCH_LIBRARY_DIRS lib64 lib/x86_64-linux-gnu local/lib64)
    endif(CMAKE_SIZEOF_VOID_P EQUAL 4)    

    find_path(LIBDAEMON_INCLUDE_DIR NAMES libdaemon/daemon.h
       PATHS       
       ${LIBDAEMON_SEARCH_INCLUDE_DIRS}
       PATH_SUFFIXES
    )

    find_library(LIBDAEMON_LIBRARY NAMES daemon
       PATHS
       ${LIBDAEMON_SEARCH_LIBRARY_DIRS}
       PATH_SUFFIXES libdaemon
    )

    include(FindPackageHandleStandardArgs)
    find_package_handle_standard_args(LIBDAEMON
            REQUIRED_VARS LIBDAEMON_INCLUDE_DIR LIBDAEMON_LIBRARY
            )

    if(LIBDAEMON_FOUND)
        set(LIBDAEMON_LIBRARIES ${LIBDAEMON_LIBRARY})
        set(LIBDAEMON_INCLUDE_DIRS ${LIBDAEMON_INCLUDE_DIR})
    endif(LIBDAEMON_FOUND)
endif(NOT LIBDAEMON_FOUND)

