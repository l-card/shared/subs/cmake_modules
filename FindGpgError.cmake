# - Try to find libgpg-error
# Once done this will define
# GPGERROR_FOUND - System has libgpg-error
# GPGERROR_INCLUDE_DIRS - The libgpg-error include directories
# GPGERROR_LIBRARIES - The libraries needed to use libgpg-error
# GPGERROR_DEFINITIONS - Compiler switches required for using libgpg-error

find_path ( GPGERROR_INCLUDE_DIR gpg-error.h )
find_library ( GPGERROR_LIBRARY NAMES gpg-error)

set ( GPGERROR_LIBRARIES ${GPGERROR_LIBRARY} )
set ( GPGERROR_INCLUDE_DIRS ${GPGERROR_INCLUDE_DIR} )

include ( FindPackageHandleStandardArgs )

find_package_handle_standard_args ( libgpg-error DEFAULT_MSG GPGERROR_LIBRARY GPGERROR_INCLUDE_DIR )